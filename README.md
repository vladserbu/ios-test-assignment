
# Touch Surgery - iOS QA Assignment
Instructions:
This assignment will test your skills with XCUITEST and Swift language.

1. Clone this repository
2. Build the App
3. Using Swift, create UI  tests that verify the following app functionality:

- Add item
- Delete item
- Update item
- Mark as Complete item
 
 `After you finish, zip all your source code and email us back or commit to a public github repository`
